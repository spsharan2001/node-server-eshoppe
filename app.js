import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import indexRouter from './src/routes/index';
import userRouter from './src/routes/user';
import accountRouter from './src/routes/account';
import productsRouter from './src/routes/products';
import brandsRouter from './src/routes/brands';
import userlistRouter from './src/routes/userList';
import loginUserRouter from './src/routes/loginUser';

let app = express();
let PORT = process.env.PORT || 9000;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/account', accountRouter);
app.use('/products', productsRouter);
app.use('/brands', brandsRouter);
app.use('/userlist', userlistRouter);
app.use('/loginUser', loginUserRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.listen(PORT);

export default app;
