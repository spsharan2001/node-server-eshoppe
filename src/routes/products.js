import express from 'express';
import productsList from '../data/productsList';

let Products = express.Router();

Products.get('/', async (req, res) => {
  res.status(200).json(productsList);
});

Products.get('/products', async (req, res) => {
  res.status(200).json(productsList);
});
export default Products;
