import express from 'express';
import popularBrands from '../data/popularBrands';

let Brands = express.Router();

Brands.get('/', async (req, res) => {
  res.status(200).json(popularBrands);
});

Brands.get('/brands', async (req, res) => {
  res.status(200).json(popularBrands);
});
export default Brands;
