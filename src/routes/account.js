import express from 'express';
import accountList from '../data/accountList';
import accountDetails from '../data/accountDetails';

let Account = express.Router();

Account.get('/', async (req, res) => {
  res.status(200).json(accountList);
});

Account.get('/details', async (req, res) => {
  res.status(200).json(accountDetails);
});
export default Account;
