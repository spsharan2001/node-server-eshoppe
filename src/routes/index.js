import express from 'express';
import '../database/models';

let router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
  res.send('<h1>eShoppe - Node Server App</h1>');
});

module.exports = router;
