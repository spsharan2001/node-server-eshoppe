import express from 'express';
import loginUser from '../data/loginUser';

let LoginUser = express.Router();

LoginUser.get('/', async (req, res) => {
  res.status(200).json(loginUser);
});

LoginUser.get('/login', async (req, res) => {
  res.status(200).json(loginUser);
});
export default LoginUser;
