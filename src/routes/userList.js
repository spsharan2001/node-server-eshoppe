import express from 'express';
import userList from '../data/userList';

let UserList = express.Router();

UserList.get('/', async (req, res) => {
  res.status(200).json(userList);
});

UserList.get('/userList', async (req, res) => {
  res.status(200).json(userList);
});
export default UserList;
