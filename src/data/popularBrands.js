const popularBrands = [
  {
    'brand_id': 'PB001',
    'brand_name': 'D&G DOLCE & GABBANA'
  },
  {
    'brand_id': 'PB002',
    'brand_name': 'NIKE'
  },
  {
    'brand_id': 'PB003',
    'brand_name': 'adidas'
  },
  {
    'brand_id': 'PB004',
    'brand_name': 'LOUIS VUITTON'
  },
  {
    'brand_id': 'PB005',
    'brand_name': 'Ray - Ban'
  },
  {
    'brand_id': 'PB006',
    'brand_name': 'PUMA'
  }
];
export default popularBrands;
